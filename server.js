var http = require("http");
var https = require("https");
var fs = require("fs");
var mongo = require("./database/mongodb");

// Express Application
var ExpressApp  = require("./app");

// Read Certificate and Private Key
const CERT = fs.readFileSync("./ssl/certificate.crt").toString();
const KEY = fs.readFileSync("./ssl/privatekey.key").toString();

// Create HTTP Server
var HttpServer = http.createServer(ExpressApp);
// Create HTTPS Server
var HttpsServer = https.createServer({cert: CERT, key: KEY}, ExpressApp);

// Connect to database
mongo.Connect();
// Start HTTP Server
HttpServer.listen(80, "0.0.0.0",() => console.log("HTTP Server - 0.0.0.0:80"));
// Start HTTPS Server
HttpsServer.listen(443, "0.0.0.0",() => console.log("HTTPS Server - 0.0.0.0:443"));