var express = require("express");
var bodyparser = require("body-parser");
var RouterAdd = require("./routes/add")
var RouterList = require("./routes/list")

// Create Express App
var ExpressApp = new express();

// Body Parser middleware
ExpressApp.use(bodyparser.json());

// Router for /add
ExpressApp.use("/add", RouterAdd);
// Router for /list
ExpressApp.use("/list", RouterList);

// Send status code 500 if there are errors
ExpressApp.use((err, req, res, next) => {
    console.log(err);
    res.status(500);
    res.send("Internal Server Error");
});

module.exports = ExpressApp;