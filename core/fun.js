function GetCombinations(array, size, start, initialStuff, output) {
    if (initialStuff.length >= size) {
        output.push(initialStuff);
    } else {
        var i;
		
        for (i = start; i < array.length; ++i) {	
            GetCombinations(array, size, i + 1, initialStuff.concat(array[i]), output);
        }
    }
}

function GetAllCombinatinos(array, size, output) {
    GetCombinations(array, size, 0, [], output);
}

exports.fun =  function(strA, strB) {
    var combinations = [];
    GetAllCombinatinos(strA.split(""),strB.length, combinations);

    for(var i = 0; i < combinations.length; i++) {
        if(combinations[i].join("") == strB) return true;
    }

    return false;
}