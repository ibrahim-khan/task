var mongodb = require("mongodb");

const URL = "mongodb://localhost:27017/";
const DATABASE = "durbinlabs";
const COLLECTION = "list";

var MDBclient;
var MDBdb;

/**
 * @function Connect Connect to the database
 */
exports.Connect = function () {
    mongodb.MongoClient.connect(URL, { useNewUrlParser: true, autoReconnect: true, useUnifiedTopology: true}).then(client => {
        MDBclient = client;
        MDBdb = client.db(DATABASE);
        console.log("Connected to Database");
    }).catch(err => {
        throw new Error("Could not initiate database");
    });
}

/**
 * @function Disconnect Disconnect from the database
 */
exports.Disconnect = function() {
    MDBclient.close();
}

/**
 * @function GetAllList Get all pairs in array
 * @returns `Promise<[]>`
 */
exports.GetAllList = function() {
    return MDBdb.collection(COLLECTION).find({}, {projection: {strA:1, strB:1, _id: 0}}).toArray();
}

/**
 * @function Add Add to the list
 */
exports.Add = function(param) {
    MDBdb.collection(COLLECTION).insertOne(param);
}