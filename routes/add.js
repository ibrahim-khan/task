var express = require('express');
var mongo = require("../database/mongodb");
var task = require("../core/fun");

var router = express.Router();

/**
 * @route `/`
 * @description Add to list for the given condition
 */
router.post("/", (req, res, next) => {
    var ret = {
        result: task.fun(req.body.strA, req.body.strB)
    };

    if(ret.result) mongo.Add(req.body);

    res.json(ret);
    next();
});

module.exports = router;