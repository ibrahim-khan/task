var express = require('express');
var mongo = require("../database/mongodb");

var router = express.Router();
/**
 * @route `/`
 * @description Get list of all stored inputs
 */
router.get("/", (req, res, next) => {
    mongo.GetAllList().then(value => res.json(value)).catch(err => req.json([]));
});

module.exports = router;